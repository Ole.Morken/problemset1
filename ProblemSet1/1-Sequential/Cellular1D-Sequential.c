/***************************************************************************************************
 * Copyright 2019 Ole Magnus Morken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
 * associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ****************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXCHAR 2000
#define EXIT_FAILURE 1


void readTable(char* name, int table[8]);
int readTest(char* name, int **bstring);
int mod(int x, int y);
int lookup(int *table, int fst, int mid, int lst);
void transform(int **hist, int *table, int n, int iter);
void resultToFile(int **hist, int m, int n);


/*******************************************************************************
 * 1D Cellular Automaton.
 * Takes input:
 *  1st) file containing a transformation table.
 *  2nd) file containing a configuration boolean string.
 *  3rd) an integer of how many iterations to perform.
 * Returns:
 *  A file containing all iterations of the initial configuration.
 *  Must be commented in first.
 * 
 * *****************************************************************************/
int main(int argc, char **argv) {    
    int **hist, table[8], *bstring, t, n;
    
    t = atoi(argv[argc-1]);

    readTable(argv[1], table);

    n = readTest(argv[2], &bstring);

    hist = malloc((t+1) * sizeof(int*));
    hist[0] = bstring;
    for(int i = 1; i < t+1; i++)
    {
        hist[i] = malloc(n * sizeof(int));
    }

    transform(hist, table, n, t);

    //Comment in if the result is of interest.
    //resultToFile(hist, t, n);

    free(hist);
    free(bstring);

    
    return 0;
}

void readTable(char* name, int table[8]) {
    long binary;
    int i;
    char line[7], *bstr;

    FILE *fp = fopen(name, "r");
    if (fp == NULL) {
            perror("fopen");
            exit(EXIT_FAILURE);
    }

    while (fgets(line, 7, fp) != NULL) {
        binary = strtol(line, &bstr, 2);
        if ('1' == bstr[1]) {
            i = 1;
        }
        else {
            i = 0;
        }
        table[binary] = i;
    }
    fclose(fp);

}

int readTest(char* name, int **bstring) {
    char line[MAXCHAR], *binary;
    int n;    

    FILE *fp = fopen(name, "r");
    
    if (fp == NULL) {
            perror("fopen");
            exit(EXIT_FAILURE);
    }

    if (fgets(line, MAXCHAR, fp) != NULL) {
        n = atoi(line);
    }

    binary = malloc((n+1) * sizeof(char));
    *bstring = malloc(n * sizeof(int));

    if (fgets(binary, n + 1, fp) != NULL) {
        for (int i = 0; i < n; i++) {
            (*bstring)[i] = (int) binary[i] - 48;
        }
    }
    fclose(fp);
    free(binary);
    return n;
}

void transform(int **hist, int *table, int n, int iter) {

    for(int i = 1; i < iter; i++)
    {
        for(int j = 0; j < n; j++)
        {
            hist[i][j] = lookup(table, hist[i-1][mod(j-1, n)], hist[i-1][j], hist[i-1][mod(j+1, n)]);
        }
    }
}

int mod(int x, int y) {
    return (x % y + y) % y;
}

int lookup(int *table, int fst, int mid, int lst) {
    int find;

    find = fst*4 + mid*2 + lst;
    
    return table[find];
}

void resultToFile(int **hist, int m, int n) {
    FILE *fp = fopen("result.txt", "w");

    if (fp == NULL) {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    for(int i = 0; i < m; i++) 
    {
        for(int j = 0; j < n; j++)
        {
            fprintf(fp, "%d", hist[i][j]);
        }
        fprintf(fp, "\n");
    }
}