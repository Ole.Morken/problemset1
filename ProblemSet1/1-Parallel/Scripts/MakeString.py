import sys

k = int(sys.argv[1])

n = 2**k * 2

fp = open("bString.txt", "w")
fp.write(str(n) + "\n")

for x in range(n//2):
    fp.write("0")

fp.write("1")

for y in range(n//2 - 1):
    fp.write("0")
