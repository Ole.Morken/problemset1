import sys
from random import randint

n = int(sys.argv[1])

fp = open("GameOfLife.txt", "w")
fp.write(str(n) + "\n")
for i in range(n):
    for j in range(n):
        fp.write(str(randint(0, 1)))
    fp.write("\n")