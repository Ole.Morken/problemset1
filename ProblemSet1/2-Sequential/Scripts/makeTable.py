
def f(bstring):
    n = 0
    for x in range(len(bstring)):
        n += int(bstring[x])
    n -= int(bstring[4])

    if (int(bstring[4]) == 1):
        if (n < 2):
            return 0
        elif (n > 3):
            return 0
        else:
            return 1
    else:
        if (n == 3):
            return 1
        else:
            return 0


fp = open("table.txt", "w")

for x in range(512):
    b = int(format(x, "b"))
    b = format(b, "0>9d")
    fp.write(b + " " + str(f(b)) + "\n")
    
    
