/***************************************************************************************************
 * Copyright 2019 Ole Magnus Morken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
 * associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ****************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#define MAXCHAR 1000

void readTable(char* name, int table[512]);
int readTest(char* name, int ***board);
int mod(int x, int y);
int lookup(int *table, int *bstring, int n);
void transform(int **board, int *table, int n, int iter);
void neighbours(int x, int y, int n, int **bstring, int **board);


/***********************************************************************************************
 * 2D Cellular automaton. The automaton simulates the Game of Life 
 * represented as an n x n matrinx.
 * Takes input:
 *  1st) file containing all possible sequences of neighbouring cells..
 *  2nd) file containing a n x n matrix descriping an initial state of GoL.
 *  3rd) an integer of how many iterations to perform the game.
 * Returns:
 *  Time taken to perform the game.
 ***********************************************************************************************/
int main(int argc, char** argv) {
    int t, n, table[512], **board;

    clock_t start, end;


    t = atoi(argv[argc-1]);

    readTable(argv[1], table);

    n = readTest(argv[2], &board);

    start = clock();

    transform(board, table, n, t);

    end = clock();

    double time = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("%.2fs", time);

    free(board);

    return 0;
}

void transform(int **board, int *table, int n, int iter) {
    int j, k, cell, bstring[9], hist[n][n];
    int nb, dj, dk;

    for(size_t s = 0; s < n; s++)
    {
        for(size_t z = 0; z < n; z++)
        {
            hist[s][z] = board[s][z];
        }
        
    }

    for(int i = 1; i < iter; i++)
    {
        if (i % 2 != 0) {
            for(j = 0; j < n; j++) 
            {
                for(k = 0; k < n; k++)
                {          
                    nb = 0;

                    for(dj = j - 1; dj <= j + 1; dj++)
                    {
                        for(dk = k - 1; dk <= k + 1; dk++)
                        {
                            bstring[nb++] = hist[mod(dj, n)][mod(dk, n)];
                        }
                    }
                    board[j][k] = lookup(table, bstring, 9);
                }
            }
        }
        else {
            for(j = 0; j < n; j++) 
            {
                for(k = 0; k < n; k++)
                {               
                    nb = 0;

                    for(dj = j - 1; dj <= j + 1; dj++)
                    {
                        for(dk = k - 1; dk <= k + 1; dk++)
                        {
                            bstring[nb++] = board[mod(dj, n)][mod(dk, n)];
                        }
                    }
                    hist[j][k] = lookup(table, bstring, 9);
                }
            }
        }
    }
}

void neighbours(int x, int y, int n, int **bstring, int **board) {
    int nb = 0;

    for(int i = x - 1; i <= x + 1; i++)
    {
        for(int j = y - 1; j <= y + 1; j++)
        {
            (*bstring)[nb++] = board[mod(x, n)][mod(y, n)];
        }
    }
}

int mod(int x, int y) {
    return (x % y + y) % y;
}

int lookup(int *table, int *bstring, int n) {
    int find = 0;

    for(int i = n - 1; i >= 0; i--)
    {
        find += bstring[i] * pow(2, i);
    }
    return table[find];
}


int readTest(char* name, int ***board) {
    char line[MAXCHAR], *binary;
    int n;    

    FILE *fp = fopen(name, "r");
    
    if (fp == NULL) {
            perror("fopen");
            exit(EXIT_FAILURE);
    }

    if (fgets(line, MAXCHAR, fp) != NULL) {
        n = atoi(line);
    }

    binary = malloc((n + 1) * sizeof(char));
    *board = malloc(n * sizeof(int*));

    for(size_t j = 0; j < n; j++)
    {
        (*board)[j] = malloc(n * sizeof(int));
    }
    
    int j = 0;
    while (fgets(binary, n + 1, fp) != NULL) {
        if (*binary != '\n') {
            for(size_t i = 0; i < n; i++)
            {      
                ((*board)[j])[i] = (int) binary[i] - 48;
            }
            j++;
        }
    }

    free(binary);

    fclose(fp);
    
    return n;
}

void readTable(char* name, int table[512]) {
    long binary;
    int i;
    char line[13], *bstr;

    FILE *fp = fopen(name, "r");
    if (fp == NULL) {
            perror("fopen");
            exit(EXIT_FAILURE);
    }

    while (fgets(line, 13, fp) != NULL) {
        binary = strtol(line, &bstr, 2);
        if ('1' == bstr[1]) {
            i = 1;
        }
        else {
            i = 0;
        }
        table[binary] = i;
    }

    fclose(fp);
}