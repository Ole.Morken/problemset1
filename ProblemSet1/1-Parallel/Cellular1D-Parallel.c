/***************************************************************************************************
 * Copyright 2019 Ole Magnus Morken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
 * associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ****************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

#define MAXCHAR 1000

//Reads a transformation table from file.
void readTable(char* name, int table[8]);

//Reads a configuration string from file.
int readTest(char* name, int **bstring);

//Modulo
int mod(int x, int y);

//Performs lookup in the tranformation table.
int lookup(int *table, int fst, int mid, int lst);

//Transform a configuration string based on transformation table.
void transform(int rank, int comm_sz, int **hist, int *table, int n, int iter);


/*******************************************************************************
 * Parallelized version of a Cellular 1D Automaton.
 * Takes input:
 *  1st) file containing a transformation table.
 *  2nd) file containing a configuration boolean string.
 *  3rd) an integer of how many iterations to perform.
 * Returns:
 *  The time taken to achieve the last configuration.
 *  Time is measured by a timer started before MPI_Scatterv in rank 0,
 *  and ends after MPI_Gatherv in rank 0.
 * 
 * The program will NOT WORK if the number of processes are more than the length
 * of HALF the configuration string, or LONGER than the string. 
 * 
 * 
 * NB: 
 *  Contains a critical bug. Should not be run as is.
 * *****************************************************************************/
int main(int argc, char **argv) {
    int comm_sz, my_rank, *sendcounts, *displs, *rec_buf;

    int **total_hist, **local_hist, table[8], *bstring, n;

    double start, end;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    sendcounts = malloc(sizeof(int)*comm_sz);
    displs = malloc(sizeof(int)*comm_sz);

    int t = atoi(argv[3]);

    /*****************************************************
     * Read all input.
     * **************************************************/
    if (my_rank == 0) {
        readTable(argv[1], table);

        n = readTest(argv[2], &bstring);
        
        total_hist = (int**) malloc((t+1) * sizeof(int*));
        total_hist[0] = bstring;

        for(int i = 1; i < t + 1; i++)
        {
            total_hist[i] = (int*) malloc(n * sizeof(int));
        }

        int rem = n % comm_sz;
        int sum = 0;

        
        for(int j = 0; j < comm_sz; j++)
        {
            sendcounts[j] = n / comm_sz;
            if (rem > 0) {
                sendcounts[j]++;
                rem--;
            }

            displs[j] = sum;
            sum += sendcounts[j];
        } 
        
    } 

    MPI_Bcast(table, 8, MPI_INT, 0, MPI_COMM_WORLD);

    MPI_Bcast(sendcounts, comm_sz, MPI_INT, 0, MPI_COMM_WORLD);

    rec_buf = (int*) malloc(sizeof(int)*sendcounts[my_rank]);

    if (my_rank == 0) {
        start = MPI_Wtime();
    }

    MPI_Scatterv(bstring, sendcounts, displs, MPI_INT, rec_buf, sendcounts[my_rank], MPI_INT, 0, MPI_COMM_WORLD);

    free(bstring);

    local_hist = malloc(sizeof(int*) * (t + 1));
    local_hist[0] = rec_buf;

    //free(rec_buf);

    for(int i = 1; i < t + 1; i++)
    {
        local_hist[i] = malloc(sizeof(int) * sendcounts[my_rank]);
    }

    transform(my_rank, comm_sz, local_hist, table, sendcounts[my_rank], t);
    
    for (int c = 1; c < t; c++)
    {
        MPI_Gatherv(local_hist[c], sendcounts[my_rank], MPI_INT, total_hist[c], sendcounts, displs, MPI_INT, 0, MPI_COMM_WORLD);
    }

    if(my_rank == 0) {
        end = MPI_Wtime();
        
        printf("The program ran for %.2fs with row size: %d\n", end - start, n);

        /*
        for(size_t i = 0; i < t; i++)
        {
            for(size_t j = 0; j < n; j++)
            {
                printf("%d", total_hist[i][j]);
            }
            printf("\n");
        }
        */
        free(total_hist);
        free(sendcounts);
        free(displs);
    }
    
    
    free(local_hist);

    MPI_Finalize();

    return 0;
}

/**********************************************************************************
 * Calculate the next t configurations of the initial configuration string.
 * ********************************************************************************/
void transform(int rank, int comm_sz, int **hist, int *table, int n, int iter) {
    int r, l, lft, rght;

    for(int i = 1; i < iter; i++)
    {   
        lft = mod((rank - 1), comm_sz);
        rght = mod((rank + 1), comm_sz);
        

        /****************************************************************************
         * Send and recieve neighbouring boolean values.
         ****************************************************************************/
        if (rank % 2 == 0) {    
            MPI_Send(hist[i - 1], 1, MPI_INT, lft, 0, MPI_COMM_WORLD);
            
            MPI_Recv(&l, 1, MPI_INT, lft, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            MPI_Send(hist[i - 1] + n - 1, 1, MPI_INT, rght, 0, MPI_COMM_WORLD);

            MPI_Recv(&r, 1, MPI_INT, rght, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        else {
            MPI_Recv(&r, 1, MPI_INT, rght, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            MPI_Send(hist[i - 1] + n - 1, 1, MPI_INT, rght, 0, MPI_COMM_WORLD);

            MPI_Recv(&l, 1, MPI_INT, lft, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            MPI_Send(hist[i - 1], 1, MPI_INT, lft, 0, MPI_COMM_WORLD);
        }

        /******************************************************************************
         * Perform the lookup for the next configuration.
         * ****************************************************************************/

        hist[i][0] = lookup(table, l, hist[i-1][0], hist[i-1][1]);
        hist[i][n - 1] = lookup(table, hist[i-1][n - 2], hist[i-1][n - 1], r);

        for(int j = 1; j < n - 1; j++)
        {
            hist[i][j] = lookup(table, hist[i-1][mod(j-1, n)], hist[i-1][j], hist[i-1][mod(j+1, n)]);
        }
    }
}

/******************************
 * Modulo
 * ****************************/
int mod(int x, int y) {
    return (x % y + y) % y;
}

/*************************************************
 * Finds the next boolean based on the rules
 * **********************************************/

int lookup(int *table, int fst, int mid, int lst) {
    int find;

    find = fst*4 + mid*2 + lst;
    
    return table[find];
}

/********************************************
 * Read the transformation rules from a file.
 * Put the rules in a table.
 * ******************************************/

void readTable(char* name, int table[8]) {
    long binary;
    int i;
    char line[7], *bstr;

    FILE *fp = fopen(name, "r");
    if (fp == NULL) {
            perror("fopen");
            exit(EXIT_FAILURE);
    }

    while (fgets(line, 7, fp) != NULL) {
        binary = strtol(line, &bstr, 2);
        if ('1' == bstr[1]) {
            i = 1;
        }
        else {
            i = 0;
        }
        table[binary] = i;
    }

    fclose(fp);
}
/******************************************
 * Read a configuration string from a file.
 * ****************************************/

int readTest(char* name, int **bstring) {
    char line[MAXCHAR], *binary;
    int n;    

    FILE *fp = fopen(name, "r");
    
    if (fp == NULL) {
            perror("fopen");
            exit(EXIT_FAILURE);
    }

    if (fgets(line, MAXCHAR, fp) != NULL) {
        n = atoi(line);
    }

    binary = malloc((n + 2) * sizeof(char));
    *bstring = malloc(n * sizeof(int));

    if (fgets(binary, n + 2, fp) != NULL) {
        for (int i = 0; i < n; i++) {
            (*bstring)[i] = (int) binary[i] - 48;
        }
    }
    fclose(fp);

    free(binary);

    return n;
}