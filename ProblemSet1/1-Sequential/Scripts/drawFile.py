import matplotlib.pyplot as plt
import numpy as np
import sys

fp = open(sys.argv[1], "r")

contents = []
x = 0
y = 0

for line in fp:
    x += 1
    for ch in line:
        if (ch != '\n'):
            y += 1
            contents.append(int(ch))

plt.imsave("visualize.jpeg", np.array(contents).reshape(x, y//x), cmap='Greys')
plt.show()