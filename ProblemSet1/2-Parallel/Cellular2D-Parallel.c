/***************************************************************************************************
 * Copyright 2019 Ole Magnus Morken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
 * associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ****************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <mpi.h>

#define MAXCHAR 1000

//Read the tranformation rules from file
void readTable(char* name, int table[512]);

//Read the initial state of Game of Life
int readTest(char* name, int **board);

//Modulo
int mod(int x, int y);

//Check the next state of a cell.
int lookup(int *table, int *bstring, int n);

//Calculate the next iteration of the Game of Life.
void transform(int my_rank, int row_sz, int comm_sz, int **board, int *table, int n, int iter);

//Generates a randomized Game of Life.
void generateBoard(int *board, int n);


/***********************************************************************************************
 * Parallelized version of a 2D Cellular automaton. The automaton simulates the Game of Life 
 * represented as an n x n matrinx.
 * Takes input:
 *  1st) file containing all possible sequences of neighbouring cells..
 *  2nd) integer n to generate an n x n board of randomized 1's and 0's, 
 *       or a file containing a n x n matrix descriping an initial state of GoL.
 *  3rd) an integer of how many iterations to perform the game.
 * Returns:
 *  The time taken to achieve the last state of The game of life.
 *  Time is measured by a timer started before MPI_Scatterv in rank 0,
 *  and ends after MPI_Gatherv in rank 0.
 * 
 * The program will NOT work if the number of processes is larger than the number of rows.
 ***********************************************************************************************/
int main(int argc, char **argv) {
    int t, n, table[512], *final_board, **local_board;
    
    int comm_sz, row_sz, my_rank, *sendcounts, *displs, *rec_buf;

    double start, end;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    sendcounts = malloc(sizeof(int)*comm_sz);
    displs = malloc(sizeof(int)*comm_sz);
    
    t = atoi(argv[argc-1]);

    if (my_rank == 0) {
        readTable(argv[1], table);

        /***************************************************
         * Reads a initial state of Game of Life from file.
         * Is not used when a board is generated randomly.
         * *************************************************/
        //n = readTest(argv[2], &final_board);

        /***********************************************
         * The number used to generate the n x n matrix.
         * ********************************************/
        n = atoi(argv[2]);

        final_board = malloc (sizeof(int) * n * n);

        generateBoard(final_board, n);

        int rem = n % comm_sz;
        int sum = 0;

        
        for(int j = 0; j < comm_sz; j++)
        {
            sendcounts[j] = n * (n / comm_sz);
            if (rem > 0) {
                sendcounts[j] += n;
                rem--;
            }

            displs[j] = sum;
            sum += sendcounts[j];
        } 
        row_sz = n;
    }

    MPI_Bcast(table, 512, MPI_INT, 0, MPI_COMM_WORLD);

    MPI_Bcast(&row_sz, 1, MPI_INT, 0, MPI_COMM_WORLD);

    MPI_Bcast(sendcounts, comm_sz, MPI_INT, 0, MPI_COMM_WORLD);

    rec_buf = malloc(sizeof(int)*sendcounts[my_rank]);

    if (my_rank == 0) {
        start = MPI_Wtime();
    }

    MPI_Scatterv(final_board, sendcounts, displs, MPI_INT, rec_buf, sendcounts[my_rank], MPI_INT, 0, MPI_COMM_WORLD);

    /************************************
     * MAKE LOCAL BOARD
     * *********************************/
    local_board = malloc(sizeof(int*) * (sendcounts[my_rank]/row_sz + 2));

    for(size_t i = 1; i < sendcounts[my_rank]/row_sz + 1; i++)
    {
        local_board[i] = rec_buf+(i-1)*row_sz;
    }
    /*************************************/

    
    /***************************************************************************
     * Perform iterations of Game of Life locally
     * ************************************************************************/
    
    transform(my_rank, row_sz, comm_sz, local_board, table, sendcounts[my_rank], t);
    /**************************************************************************/

    MPI_Gatherv(local_board[1], sendcounts[my_rank], MPI_INT, final_board, sendcounts, displs, MPI_INT, 0, MPI_COMM_WORLD);

    free(rec_buf);
    free(local_board);

    if(my_rank == 0) {
        end = MPI_Wtime();
        printf("The program ran for %.2fs with row size: %d\n", end - start, n);

        /*******************************************
         * Print the final iteration of Game of Life
         * *****************************************//*
        for(size_t o = 0; o < n * n; o++)
        {
            if(o % n == 0 && o > 0) {
                printf("\n");
            }
            printf("%d", final_board[o]);
        }
        printf("\n");
        /*******************************************/    
        
        free(final_board);
    }

    MPI_Finalize();

    free(sendcounts);
    free(displs);
    
    return 0;
}


/************************************************************************************************
 * Simulates the t-next states of Game of Life.
 * *********************************************************************************************/
void transform(int my_rank, int row_sz, int comm_sz, int **board, int *table, int n, int iter) {
    int rows = n / row_sz;

    int **alt_board, bstring[9];
    int *up_row, *down_row;
    int *rec_up, *rec_down;
    int up, down, nb, da, db;
    int a, b;

    up = mod(my_rank - 1, comm_sz);
    down = mod(my_rank + 1, comm_sz);

    alt_board = malloc(sizeof(int*) * (rows + 2));

    for(size_t l = 0; l < rows + 2; l++)
    {
        alt_board[l] = malloc(sizeof(int) * row_sz);
    }

    rec_up = malloc(sizeof(int) * row_sz);
    rec_down = malloc(sizeof(int) * row_sz);

    for(size_t t = 1; t < iter; t++)
    {
        if (t % 2 != 0) {
            up_row = board[1];
            down_row = board[rows];
        }
        else {
            up_row = alt_board[1];
            down_row = alt_board[rows];
        }

        if (my_rank % 2 == 0) {
            MPI_Send(up_row, row_sz, MPI_INT, up, 0, MPI_COMM_WORLD);
            MPI_Recv(rec_down, row_sz, MPI_INT, down, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Send(down_row, row_sz, MPI_INT, down, 0, MPI_COMM_WORLD);
            MPI_Recv(rec_up, row_sz, MPI_INT, up, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        else {
            MPI_Recv(rec_down, row_sz, MPI_INT, down, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Send(up_row, row_sz, MPI_INT, up, 0, MPI_COMM_WORLD);
            MPI_Recv(rec_up, row_sz, MPI_INT, up, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Send(down_row, row_sz, MPI_INT, down, 0, MPI_COMM_WORLD);
        }
        /************************************************
         * ODD ITERATION
         * Calculate next iteration of the Game of Life
         * ***********************************************/
        if (t % 2 != 0) 
        {
            board[0] = rec_up;
            board[rows + 1] = rec_down;

            for(a = 1; a < rows + 1; a++)
            {
                for(b = 0; b < row_sz; b++)
                {
                    /****************************************************************************
                     * Find Neighbours and make the binary string for lookup
                     * ************************************************************************/
                    nb = 0;
                    
                    for(da = a - 1; da <= a + 1; da++)
                    {
                        for(db = b - 1; db <= b + 1; db++)
                        {
                            bstring[nb++] = board[mod(da, row_sz)][mod(db, row_sz)];
                        }
                    }
                    /**********************************************************************/
                    alt_board[a][b] = lookup(table, bstring, 9);
                }
            }
        }
        /*******************************************************/
        /************************************************
         * EVEN ITERATION
         * Calculate next iteration of the Game of Life
         * ***********************************************/
        else
        {
            alt_board[0] = rec_up;
            alt_board[rows + 1] = rec_down;
            for(a = 1; a < rows + 1; a++)
            {
                for(b = 0; b < row_sz; b++)
                {
                    /****************************************************************************
                     * Find Neighbours and make the binary string for lookup
                     * ************************************************************************/
                    nb = 0;

                    for(da = a - 1; da <= a + 1; da++)
                    {
                        for(db = b - 1; db <= b + 1; db++)
                        {
                            bstring[nb++] = alt_board[mod(da, row_sz)][mod(db, row_sz)];
                        }
                    }
                    /**********************************************************************/
                    board[a][b] = lookup(table, bstring, 9);
                }
            }
        }
        /*******************************************************/
        
    }
    if (iter % 2 != 0) {
        for(size_t i = 0; i < rows + 2; i++)
        {
            for(size_t j = 0; j < row_sz; j++)
            {
                board[i][j] = alt_board[i][j];
            } 
        }
        
    }
    free(alt_board);
    free(rec_up);
    free(rec_down);

}

int lookup(int *table, int *bstring, int n) {
    int find = 0;
    int p = 1;

    for(int i = n - 1; i >= 0; i--)
    {
        find += p * bstring[i];
        p = 2 * p;
    }

    return table[find];
}


int mod(int x, int y) {
    return (x % y + y) % y;
}

int readTest(char* name, int **board) {
    char line[MAXCHAR], *binary;
    int n;    

    FILE *fp = fopen(name, "r");
    
    if (fp == NULL) {
            perror("fopen");
            exit(EXIT_FAILURE);
    }

    if (fgets(line, MAXCHAR, fp) != NULL) {
        n = atoi(line);
    }

    binary = malloc((n + 1) * sizeof(char));
    *board = malloc(n* n * sizeof(int));

    
    int j = 0;
    while (fgets(binary, n + 2, fp) != NULL) {
        if (*binary != '\n') {
            for(size_t i = 0; i < n; i++)
            {
                (*board)[j++] = (int) binary[i] - 48;
            }
        }
    }

    free(binary);

    fclose(fp);
    
    return n;
}

void readTable(char* name, int table[512]) {
    long binary;
    int i;
    char line[13], *bstr;

    FILE *fp = fopen(name, "r");
    if (fp == NULL) {
            perror("fopen");
            exit(EXIT_FAILURE);
    }

    while (fgets(line, 13, fp) != NULL) {
        binary = strtol(line, &bstr, 2);
        if ('1' == bstr[1]) {
            i = 1;
        }
        else {
            i = 0;
        }
        table[binary] = i;
    }

    fclose(fp);
}

void generateBoard(int *board, int n) {
    for(size_t i = 0; i < n; i++)
    {
        for(size_t j = 0; j < n; j++)
        {
            *(board + n * i + j) = (int) mod(rand(), 2);
        }
        
    }
    
}